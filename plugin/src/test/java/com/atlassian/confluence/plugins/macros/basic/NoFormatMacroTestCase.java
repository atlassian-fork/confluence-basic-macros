package com.atlassian.confluence.plugins.macros.basic;

import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.SubRenderer;
import org.junit.Test;

import static java.util.Collections.emptyMap;
import static org.junit.Assert.assertTrue;

public class NoFormatMacroTestCase {
    /**
     * CONFDEV-1818 Ensure that the body of the macro is escaped in the macro output.
     *
     * @throws Exception
     */
    @Test
    public void testEscapedBody() throws Exception {
        NoformatMacro noformatMacro = new NoformatMacro(new MockSubRenderer());
        String output = noformatMacro.execute(emptyMap(), "<p>hello &amp; world</p>", new DefaultConversionContext(new PageContext()));

        assertTrue(output.contains("&lt;p&gt;hello &amp;amp; world&lt;/p&gt;"));
    }

    private static class MockSubRenderer implements SubRenderer {
        public String render(String originalContent, RenderContext renderContext) {
            return originalContent;
        }

        public String renderAsText(String originalContent, RenderContext context) {
            return originalContent;
        }

        public String getRendererType() {
            return null;
        }

        public String render(String wiki, RenderContext renderContext, RenderMode newRenderMode) {
            return wiki;
        }
    }
}
